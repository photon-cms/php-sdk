# PHOTONCMS PHP-SDK

## Setup
Via composer once it is published 

## Initialization
In order to use the SDK first thing you need to do is to create an instance of `Photon\Client` using the user's email and password and the API endpoint that you wish to fetch results from as arguments.
``` php
$client = new \Photon\Client('super.administrator@photoncms.test', 'L!gthsp44d', 'http://photoncms.test/api/');
```
If your project is using `.env` file you can store the arguments inside the file instead of sending them as arguments when creating a new instance.
``` php
PHOTON_EMAIL=super.administrator@photoncms.test
PHOTON_PASSWORD=L!gthsp44d
PHOTON_API_ENDPOINT=http://photoncms.local/api/

...

$client = new \Photon\Client();
```
Every response you recieve from API will be related to the user credentials you used, and will take into account their roles and permissions when retrieving data. 

## Usage
* [Locales](#locales)
* [Included Fields](#included_fields)
* [Impersonate](#impersonate)
* [Model](#model)
* [Caching](#caching)
* [Calls](#calls)

### Locales
You can set up an array of locales that will be retreived via the API. You will receive translations in those locales from every translatable field, if translations exist. If you do not set locales the default locale that is setup in your Photon CMS project will be used.
``` php
$client->setLocales(['en', 'de', 'fr']);
```
You can also check current locales set in your client using this method:
``` php
$client->getLocales();
```
### Included Fields
You can set up an array of included fields that will be used for filtering response retreived via the API. Values of this array should be `fields` of the model you are working with. Only fields specified in this array will be retreived. If you do not set included fields the entire response will be returned.
``` php
$client->setIncludedFields(['id', 'created_at', 'first_name', 'last_name']);
```
You can also check current included fields set in your client using this method:
``` php
$client->getIncludedFields();
```
### Impersonate
If your instance is created with a user that has super administrator role you'll have an option of impersonating other users. This will allow you to recieve API responses like those users would, with their own roles and permissions taken into account.
``` php
# this method will impersonate user with specific id, and every subsequent request created with this client would be made in his name
$client->impersonate($id);

# with this method you'll stop user impersonation and return to original super administrator user that initialized the client in the first place
$client->stopImpersonate();
```
### Model
Before making any API calls you'll need to specify a model that you wish to access using a model name. Failing to do so will cause an exception.
``` php
# setting up model that will be used by the client
$client->setModel('news');
# method to check which model is currently being used
$client->getModel();
```
### Caching
SDK has the built in caching system that you can use. It supports multitude of different caching backends, though some of them may require appropriate PHP extensions to be installed and enabled. More info: [`https://github.com/apix/cache`](https://github.com/apix/cache).
``` php
# method that check if caching is currently enabled
$client->cachingEnabled();
# method that enables caching for every subseqent call 
$client->enableCaching();
# method that disables caching for every subseqent call 
$client->disableCaching();
```
By default, caching will use `Files` backend implemented via `\Apix\Cache\Files` class and every key will have a ttl of 5 minutes. You can overwrite the default caching backend by sending an instance of backend you want to use as a forth argument when creating the client.
``` php
# Apcu
$apcuClient = new \Apix\Cache\Apcu();
$client = new \Photon\Client('super.administrator@photoncms.test', 'L!gthsp44d', 'http://photoncms.test/api/', $apcuClient);

# Directory
$directoryClient = new \Apix\Cache\Directory();
$client = new \Photon\Client('super.administrator@photoncms.test', 'L!gthsp44d', 'http://photoncms.test/api/', $directoryClient);

# Redis 
$redisClient = new \Apix\Cache\Redis(new \Redis);
$client = new \Photon\Client('super.administrator@photoncms.test', 'L!gthsp44d', 'http://photoncms.test/api/', $redisClient);

# Memcached 
$memcachedClient = new \Apix\Cache\Memcached(new \Memcached);
$client = new \Photon\Client('super.administrator@photoncms.test', 'L!gthsp44d', 'http://photoncms.test/api/', $memcachedClient);
```
### Calls
#### Make API call
You can make any existing API call using this method.
``` php
$photonClient = new \Photon\Client();
...
# make call to API route without sending request body or additional header parameters 
$photonClient->makeApiCall('POST', 'existing/endpoint');

# make call to API route together with request body and additional header parameters 
$photonClient->makeApiCall('POST', 'existing/endpoint', $requestBodyArray, $requestHeaderArray);
```
#### Get all
This call will get all entries from a model.
``` php
$photonClient = new \Photon\Client();
...
$photonClient->setModel('news')->getAll();
```
#### Get single
This call will get a single entry from a model.
``` php
$photonClient = new \Photon\Client();
...
$photonClient->setModel('news')->get($id);
```
#### Create
This call will create a new entry for a model.
``` php
$photonClient = new \Photon\Client();
...
$photonClient->setModel('news')->create([
	'field_1' => 'field_1_value',
	'field_2' => 'field_2_value',
	'field_3' => 'field_3_value',
]);
```
#### Update
This call will update an existing entry for a model.
``` php
$photonClient = new \Photon\Client();
...
$photonClient->setModel('news')->update($entryId, [
	'field_1' => 'field_1_updated_value',
	'field_2' => 'field_2_updated_value',
	'field_3' => 'field_3_updated_value',
]);
```
#### Delete
This call will delete an entry from a model.
``` php
$photonClient = new \Photon\Client();
...
$photonClient->setModel('news')->delete($entryId);
```
#### Filter
This call will return a filtered list of entries from a model.
``` php
$photonClient = new \Photon\Client();
...
$photonClient->setModel('news')->filter([
	'filter' => [
		'field_1' => ['equals' => 'field_1_value'],
		'field_2' => ['equals' => 'field_2_value'],
	],
	'pagination' => [
		'items_per_page' => 10,
		'current_page' => 1,
	],
	'sorting' => [
		'id' => 'desc',
	],
]);

```
#### Count
This call will return a count of entries from a model.
``` php
$photonClient = new \Photon\Client();
...
$photonClient->setModel('news')->count();

```

## Public API

If your project is using `.env` file you can store the arguments inside the file instead of sending them as arguments when creating a new instance.
``` php
PHOTON_PUBLIC_API_ENDPOINT=http://photoncms.local/public-api/
...
```

### Count
This call will return a count of entries from a model.
``` php
$photonPublicClient = new \Photon\PublicClient();
...
$photonPublicClient->setModel('news')->count();

```
#### Advance Filtering
This call will return a filtered list of entries from a model.
``` php
$photonPublicClient = new \Photon\PublicClient();
...
$photonPublicClient->setModel('news')->advanceFiltering([
	'filter' => [
		'field_1' => ['equals' => 'field_1_value'],
		'field_2' => ['equals' => 'field_2_value'],
	],
	'pagination' => [
		'items_per_page' => 10,
		'current_page' => 1,
	],
	'sorting' => [
		'id' => 'desc',
	],
]);

```
#### Get single
This call will get a single entry from a model.
``` php
$photonPublicClient = new \Photon\PublicClient();
...
$photonPublicClient->setModel('news')->get($id);
```

#### Paginate
This call will get a subset of entries from a model.
``` php
$photonPublicClient = new \Photon\PublicClient();
...
$photonPublicClient->setModel('news')->paginate($offset(=1),$count(=20));
```
