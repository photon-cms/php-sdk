<?php

namespace Photon;

use Exception;

/**
 * Photon Exception.
 */
class PhotonException extends Exception
{
}
