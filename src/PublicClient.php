<?php

namespace Photon;

use Apix\Cache\Adapter;
use GuzzleHttp\Client as Guzzle;
use GuzzleHttp\Exception\GuzzleException;
use Photon\Interfaces\ReadRecords;
use Photon\Services\PreparationService;

/**
 * Photon Public Client.
 */
class PublicClient implements ReadRecords
{
    private PreparationService $prepareService;

    private array $sort = [];

    /**
     * BaseClient constructor.
     *
     * @param string|null $apiEndpoint
     * @param Adapter|null $cache
     * @param Guzzle|null $client
     * @throws PhotonException
     */
    public function __construct(?string $apiEndpoint = null, ?Adapter $cache = null, ?Guzzle $client = null)
    {
        $this->prepareService = new PreparationService($apiEndpoint, $cache, $client);
    }

    /**
     * @return $this
     */
    public function enableCache(): self
    {
        $this->prepareService->enableCaching();

        return $this;
    }

    /**
     * @param string $model
     * @return $this
     */
    public function setModel(string $model): self
    {
        $this->prepareService->setModel($model);

        return $this;
    }

    /**
     * @param int $depth
     * @return $this
     */
    public function setDepth(int $depth): self
    {
        $this->prepareService->setDepth($depth);

        return $this;
    }

    /**
     * @param string $by
     * @return $this
     */
    public function sortAscBy(string $by): self
    {
        $this->sort = [$by => 'asc'];

        return $this;
    }

    /**
     * @param string $by
     * @return $this
     */
    public function sortDescBy(string $by): self
    {
        $this->sort = [$by => 'desc'];

        return $this;
    }

    /**
     * @return int
     * @throws GuzzleException
     */
    public function count(): int
    {
        return $this->prepareService->filter([])['pagination']['total'];
    }

    /**
     * @param int $id
     * @return array
     */
    public function get(int $id): array
    {
        return $this->prepareService->get($id);
    }

    /**
     * @param int $offset
     * @param int $count
     * @return array
     * @throws GuzzleException
     */
    public function paginate(int $offset = 1, int $count = 20): array
    {
        return $this->prepareService->filter([
            'pagination' => [
                'current_page' => $offset,
                'items_per_page' => $count
            ],
            'sorting' => $this->sort
        ]);
    }

    /**
     * @param array $filter
     * @return array
     * @throws GuzzleException
     */
    public function advanceFiltering(array $filter = []): array
    {
        return $this->prepareService->filter($filter);
    }
}
