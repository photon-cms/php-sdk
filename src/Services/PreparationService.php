<?php

namespace Photon\Services;

use Apix\Cache\Adapter;
use Apix\Cache\Files;
use GuzzleHttp\Client as Guzzle;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use Photon\PhotonException;

class PreparationService
{
    /**
     * @var string
     */
    protected string $apiEndpoint = '';

    /**
     * @var array
     */
    protected array $locales = [];

    /**
     * @var Guzzle
     */
    private Guzzle $client;

    /**
     * @var bool
     */
    private bool $caching = false;

    /**
     * @var int
     */
    private int $cachingTime = 300;

    /**
     * @var Adapter
     */
    private Adapter $cache;

    /**
     * @var string
     */
    private string $model = '';

    /**
     * @var int
     */
    private int $depth = 0;

    /**
     * BaseClient constructor.
     *
     * @param string|null $apiEndpoint
     * @param Adapter|null $cache
     * @param Guzzle|null $client
     * @throws PhotonException
     */
    public function __construct(?string $apiEndpoint = null, ?Adapter $cache = null, ?Guzzle $client = null)
    {
        $this->cache = $cache ?: new Files();

        $this->setApiEndpoint($apiEndpoint);

        $this->client = $client ?: new Guzzle([
          'base_uri' => $this->getApiEndpoint()
      ]);
    }

    /**
     * Model setter.
     *
     * @param string $model
     * @return PreparationService
     */
    public function setModel(string $model): self
    {
        $this->model = $model;

        return $this;
    }

    /**
     * Model getter.
     *
     * @return string
     */
    protected function getModel(): string
    {
        return $this->model;
    }

    /**
     * Check if caching is enabled.
     *
     * @return bool
     */
    protected function isCachingEnabled(): bool
    {
        return $this->caching;
    }

    /**
     * Enable caching of API responses.
     *
     * @return self
     */
    public function enableCaching(): self
    {
        $this->caching = true;

        return $this;
    }

    /**
     * @param int $depth
     * @return $this
     */
    public function setDepth(int $depth): self
    {
        $this->depth = $depth;

        return $this;
    }

    /**
     * Disable caching of API responses.
     *
     * @return self
     */
    protected function disableCaching(): self
    {
        $this->caching = false;

        return $this;
    }

    /**
     * Filter model entries.
     *
     * @param array $data
     *
     * @return array
     * @throws GuzzleException
     */
    public function filter(array $data): array
    {
        if (! $this->isCachingEnabled()) {
            return $this->makeApiCall('POST', 'filter', array_merge(
            [
                'modules' => [$this->getModel()]
            ], $data));
        }

        $cachingKey = md5(json_encode([
            'model'   => $this->getModel(),
            'id'      => 'filter',
            'filter'   => $data,
        ]));
        $cachedResponse = $this->cache->loadKey($cachingKey);

        if (! is_null($cachedResponse)) {
            return $cachedResponse;
        }

        $apiResponse = $this->makeApiCall('POST', 'filter', array_merge(
        [
            'modules' => [$this->getModel()]
        ], $data));
        $this->cache->save($apiResponse, $cachingKey, [$this->getModel()], $this->cachingTime);

        return $apiResponse;
    }


    /**
     * Get single entry from model.
     *
     * @param int $id
     *
     * @return array
     */
    public function get(int $id): array
    {
        if (! $this->isCachingEnabled()) {
            return $this->makeApiCall('GET', $this->getModel() . '/' . $id);
        }

        $cachingKey = json_encode([
            'model'           => $this->getModel(),
            'id'              => $id,
            'filter'           => [],
        ]);
        $cachedResponse = $this->cache->loadKey($cachingKey);

        if (! is_null($cachedResponse)) {
            return $cachedResponse;
        }

        $apiResponse = $this->makeApiCall('GET', $this->getModel() . '/' . $id);
        $this->cache->save($apiResponse, $cachingKey, [$this->getModel()], $this->cachingTime);

        return $apiResponse;
    }

    /**
     * Create API call.
     *
     * @param string $method
     * @param string $endpoint
     * @param array $data
     * @return array
     */
    private function makeApiCall(string $method, string $endpoint, array $data = []): array
    {
        try {
            $response = $this->client->request($method, $endpoint, [
                'json' => $data,
                'headers' => $this->prepareRequestHeader()
            ]);
        } catch (GuzzleException | ClientException $e) {
            $content = (string) $e->getResponse()->getBody();

            return json_decode($content, true);
        }

        $content = $response->getBody()->getContents();

        return json_decode($content, true)['body'];
    }

    /**
     * Locales setter.
     *
     * @param array $locales
     *
     * @return self
     */
    public function setLocales(array $locales): self
    {
        $this->locales = $locales;

        return $this;
    }

    /**
     * Locales getter.
     *
     * @return array
     */
    private function getLocales(): array
    {
        return $this->locales;
    }

    /**
     * Prepare header array for new request.
     *
     * @return array
     */
    private function prepareRequestHeader(): array
    {
        $requestHeader = [
            'Accept' => 'application/json',
            'Depth' => $this->depth,
        ];

        $locales = $this->getLocales();
        if ($locales) {
            $requestHeader += [
                'Locales' => implode(',', $locales),
            ];
        }

        return $requestHeader;
    }

    /**
     * @param string|null $apiEndpoint
     *
     * @return void
     * @throws PhotonException
     */
    private function setApiEndpoint(?string $apiEndpoint): void
    {
        $this->validateApiEndpoint($apiEndpoint);
    }

    /**
     * @return string
     */
    private function getApiEndpoint(): string
    {
        return $this->apiEndpoint;
    }

    /**
     * @param string|null $apiEndpoint
     *
     * @return void
     * @throws PhotonException
     */
    private function validateApiEndpoint(?string $apiEndpoint): void
    {
        $this->apiEndpoint = !empty($apiEndpoint) ? $apiEndpoint : getenv('PHOTON_PUBLIC_API_ENDPOINT');

        if (! $this->apiEndpoint) {
            throw new PhotonException('API endpoint missing', 500);
        }
    }
}
