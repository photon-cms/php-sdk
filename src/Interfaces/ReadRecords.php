<?php

namespace Photon\Interfaces;

use GuzzleHttp\Exception\GuzzleException;

interface ReadRecords
{
    /**
     * Count model entries.
     *
     * @return int
     * @throws GuzzleException
     */
    public function count(): int;

    /**
     * Get single entry from model.
     *
     * @param int $id
     *
     * @return array
     * @throws GuzzleException
     */
    public function get(int $id): array;

    /**
     * Get single entry from model.
     *
     * @param int $offset
     * @param int $count
     * @return array
     * @throws GuzzleException
     */
    public function paginate(int $offset = 1, int $count = 20): array;

    /**
     * @param array $filter
     * @return array
     * @throws GuzzleException
     */
    public function advanceFiltering(array $filter = []): array;
}
