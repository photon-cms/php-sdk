<?php

namespace Photon\Interfaces;

use GuzzleHttp\Exception\GuzzleException;

interface CRUDRecords
{
    /**
     * Get all entries from model.
     *
     * @return array
     * @throws GuzzleException
     */
    public function getAll(): array;

    /**
     * Get single entry from model.
     *
     * @param int $id
     *
     * @return array
     * @throws GuzzleException
     */
    public function get(int $id): array;

    /**
     * Filter model entries.
     *
     * @param array $data
     *
     * @return array
     * @throws GuzzleException
     */
    public function filter(array $data): array;

    /**
     * Count model entries.
     *
     * @param array $data
     *
     * @return array
     * @throws GuzzleException
     */
    public function count(array $data = []): array;

    /**
     * Create entry for model.
     *
     * @param array $data
     *
     * @return array
     * @throws GuzzleException
     */
    public function create(array $data): array;

    /**
     * Update entry for model.
     *
     * @param int $id
     * @param array $data
     *
     * @return array
     * @throws GuzzleException
     */
    public function update(int $id, array $data): array;

    /**
     * Delete entry for model.
     *
     * @param int $id
     *
     * @return array
     * @throws GuzzleException
     */
    public function delete(int $id): array;
}
