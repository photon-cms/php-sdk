<?php

namespace Photon\Traits\Auth;

use GuzzleHttp\Exception\GuzzleException;

/**
 * Photon Impersonate User trait.
 */
trait ImpersonateUser
{
    /**
     * @var string|null
     */
    private $impersonateToken;

    /**
     * Impersonate user.
     *
     * @param int $id
     *
     * @return self
     * @throws GuzzleException
     */
    public function impersonate(int $id): self
    {
        $response = $this->makeApiCall('GET', 'auth/impersonate/' . $id);

        $this->setImpersonateToken($response['body']['token']['token']);

        return $this;
    }

    /**
     * Stop impersonating user.
     *
     * @return self
     * @throws GuzzleException
     */
    public function stopImpersonate(): self
    {
        $response = $this->makeApiCall('GET', 'auth/impersonate/stop');

        $this->setToken($response['body']['token']['token']);
        $this->setImpersonateToken(null);

        return $this;
    }

    /**
     * @param string|null $token
     *
     * @return void
     */
    private function setImpersonateToken(?string $token): void
    {
        $this->impersonateToken = $token;
    }

    /**
     * @return string
     */
    private function getImpersonateToken(): ?string
    {
        return $this->impersonateToken;
    }
}
