<?php

namespace Photon\Traits\Auth;

use Exception;
use GuzzleHttp\Exception\GuzzleException;
use Photon\PhotonException;

/**
 * Photon Authenticate User trait.
 */
trait AuthenticateUser
{
    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $token;

    /**
     * @var int
     */
    private $userId;

    /**
     * Authenticate user.
     *
     * @return void
     * @throws PhotonException
     * @throws Exception
     * @throws GuzzleException
     *
     */
    private function authenticate(): void
    {
        if ($this->loadTokenFromCache()) {
            return;
        }

        $response = $this->makeApiCall('POST', 'auth/login', [
            'email'    => $this->getEmail(),
            'password' => $this->getPassword(),
        ]);

        if (! isset($response['body']['token']['token'])) {
            throw new PhotonException('Token missing', 401);
        }

        $this->saveTokenToCache($response);

        $this->setUserId($response['body']['user']['id']);
        $this->setToken($response['body']['token']['token']);
    }

    /**
     * @param string $email
     *
     * @return void
     * @throws PhotonException
     */
    private function setEmail(string $email): void
    {
        $this->validateEmail($email);
        $this->email = $email;
    }

    /**
     * @return string
     */
    private function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     *
     * @return void
     *@throws PhotonException
     *
     */
    private function validateEmail(string &$email): void
    {
        $email = $email ?: getenv('PHOTON_EMAIL');

        if (! $email) {
            throw new PhotonException('Email missing', 500);
        }
    }

    /**
     * @param string $password
     *
     * @return void
     * @throws PhotonException
     */
    private function setPassword(string $password): void
    {
        $this->validatePassword($password);
        $this->password = $password;
    }

    /**
     * @return string
     */
    private function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     *
     * @return void
     * @throws PhotonException
     *
     */
    private function validatePassword(string &$password): void
    {
        $password = $password ?: getenv('PHOTON_PASSWORD');

        if (! $password) {
            throw new PhotonException('Password missing', 500);
        }
    }

    /**
     * @param string $userId
     *
     * @return void
     */
    private function setUserId(string $userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @return int|null
     */
    private function getUserId(): ?int
    {
        return $this->userId;
    }

    /**
     * @param string $token
     *
     * @return void
     */
    private function setToken(string $token): void
    {
        $this->token = $token;
    }

    /**
     * @return string
     */
    private function getToken(): ?string
    {
        return $this->token;
    }

    /**
     * Load token from cache if it exists.
     *
     * @return bool
     */
    private function loadTokenFromCache(): bool
    {
        $photonAuthCache = $this->cache->loadKey(json_encode([
            'email'    => $this->getEmail(),
            'password' => $this->getPassword(),
        ]));

        if ($photonAuthCache) {
            $this->setUserId($photonAuthCache['id']);
            $this->setToken($photonAuthCache['token']);
            return true;
        }

        return false;
    }

    /**
     * Save token to cache.
     *
     * @param array $responseData
     *
     * @return void
     */
    private function saveTokenToCache(array $responseData): void
    {
        $this->cache->save([
            'id' => $responseData['body']['user']['id'],
            'token' => $responseData['body']['token']['token']
        ], json_encode([
            'email'    => $this->getEmail(),
            'password' => $this->getPassword(),
        ]), null, 60 * 60 * 48);
    }

    /**
     * Clear token from cache.
     *
     * @return void
     * @throws PhotonException
     */
    private function clearTokenFromCache(): void
    {
        $this->cache->delete(json_encode([
            'email'    => $this->getEmail(),
            'password' => $this->getPassword(),
        ]));

        $this->authenticate();
    }
}
