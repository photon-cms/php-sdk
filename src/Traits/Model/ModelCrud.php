<?php

namespace Photon\Traits\Model;

use GuzzleHttp\Exception\GuzzleException;
use Photon\Client;

/**
 * Photon Model Crud trait.
 */
trait ModelCrud
{
    /**
     * @var array
     */
    private array $includedFields = [];

    /**
     * @var array
     */
    private array $locales = [];

    /**
     * @var string
     */
    private string $model = '';

    /**
     * Included fields setter.
     *
     * @param array $includedFields
     *
     * @return ModelCrud|Client
     */
    public function setIncludedFields(array $includedFields): self
    {
        $this->includedFields = $includedFields;

        return $this;
    }

    /**
     * Included fields getter.
     *
     * @return array
     */
    public function getIncludedFields(): array
    {
        return $this->includedFields;
    }

    /**
     * Prepare included fields for request.
     *
     * @return string
     */
    private function prepareIncludedFields(): string
    {
        if (empty($this->includedFields)) {
            return '';
        }

        $urlSuffix = '?';
        foreach ($this->includedFields as $includedField) {
            $urlSuffix .= 'include[]=' . $includedField . '&';
        }

        return $urlSuffix;
    }

    /**
     * Locales setter.
     *
     * @param array $locales
     *
     * @return self
     */
    public function setLocales(array $locales): self
    {
        $this->locales = $locales;

        return $this;
    }

    /**
     * Locales getter.
     *
     * @return array
     */
    public function getLocales(): ?array
    {
        return $this->locales;
    }

    /**
     * Model setter.
     *
     * @param string $model
     * @return self
     */
    public function setModel(string $model): self
    {
        $this->model = $model;

        return $this;
    }

    /**
     * Model getter.
     *
     * @return string
     */
    public function getModel(): string
    {
        return $this->model;
    }

    /**
     * Get all entries from model.
     *
     * @return array
     * @throws GuzzleException
     */
    public function getAll(): array
    {
        if (! $this->cachingEnabled()) {
            return $this->makeApiCall('GET', $this->getModel());
        }

        $cachingKey = json_encode([
            'model'           => $this->getModel(),
            'id'              => 'all',
            'user_id'         => $this->getUserId(),
            'included_fields'  => $this->getIncludedFields(),
            'filter'           => [],
        ]);
        $cachedResponse = $this->cache->loadKey($cachingKey);

        if (! is_null($cachedResponse)) {
            return $cachedResponse;
        }

        $apiResponse = $this->makeApiCall('GET', $this->getModel());
        $this->cache->save($apiResponse, $cachingKey, [$this->getModel()], $this->cachingTime);

        return $apiResponse;
    }

    /**
     * Get single entry from model.
     *
     * @param int $id
     *
     * @return array
     * @throws GuzzleException
     */
    public function get(int $id): array
    {
        if (! $this->cachingEnabled()) {
            return $this->makeApiCall('GET', $this->getModel() . 'ModelCrud.php/' . $id);
        }

        $cachingKey = json_encode([
            'model'           => $this->getModel(),
            'id'              => $id,
            'user_id'         => $this->getUserId(),
            'included_fields'  => $this->getIncludedFields(),
            'filter'           => [],
        ]);
        $cachedResponse = $this->cache->loadKey($cachingKey);

        if (! is_null($cachedResponse)) {
            return $cachedResponse;
        }

        $apiResponse = $this->makeApiCall('GET', $this->getModel() . 'ModelCrud.php/' . $id);
        $this->cache->save($apiResponse, $cachingKey, [$this->getModel()], $this->cachingTime);

        return $apiResponse;
    }

    /**
     * Filter model entries.
     *
     * @param array $data
     *
     * @return array
     * @throws GuzzleException
     */
    public function filter(array $data): array
    {
        if (! $this->cachingEnabled()) {
            return $this->makeApiCall('POST', 'filter/' . $this->getModel(), $data);
        }

        $cachingKey = json_encode([
            'model'   => $this->getModel(),
            'id'      => 'filter',
            'user_id' => $this->getUserId(),
            'filter'   => $data,
        ]);
        $cachedResponse = $this->cache->loadKey($cachingKey);

        if (! is_null($cachedResponse)) {
            return $cachedResponse;
        }

        $apiResponse = $this->makeApiCall('POST', 'filter/' . $this->getModel(), $data);
        $this->cache->save($apiResponse, $cachingKey, [$this->getModel()], $this->cachingTime);

        return $apiResponse;
    }

    /**
     * Count model entries.
     *
     * @param array $data
     *
     * @return array
     * @throws GuzzleException
     */
    public function count(array $data = []): array
    {
        return $this->makeApiCall('POST', 'count/' . $this->getModel(), $data);
    }

    /**
     * Create entry for model.
     *
     * @param array $data
     *
     * @return array
     * @throws GuzzleException
     */
    public function create(array $data): array
    {
        if ($this->cachingEnabled()) {
            $this->cache->clean([$this->getModel()]);
        }

        return $this->makeApiCall('POST', $this->getModel(), $data);
    }

    /**
     * Update entry for model.
     *
     * @param int $id
     * @param array $data
     *
     * @return array
     * @throws GuzzleException
     */
    public function update(int $id, array $data): array
    {
        if ($this->cachingEnabled()) {
            $this->cache->clean([$this->getModel()]);
        }

        return $this->makeApiCall('PUT', $this->getModel() . 'ModelCrud.php/' . $id, $data);
    }

    /**
     * Delete entry for model.
     *
     * @param int $id
     *
     * @return array
     * @throws GuzzleException
     */
    public function delete(int $id): array
    {
        if ($this->cachingEnabled()) {
            $this->cache->clean([$this->getModel()]);
        }

        return $this->makeApiCall('DELETE', $this->getModel() . 'ModelCrud.php/' . $id);
    }
}
