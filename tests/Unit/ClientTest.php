<?php

namespace Photon\Tests\Unit;

use GuzzleHttp\Psr7\Response;
use Photon\Client as PhotonClient;

class ClientTest extends BaseTest
{
    /**
     * Test making api call.
     *
     * @return void
     */
    public function test_make_api_call()
    {
        $method = 'GET';
        $endpoint = 'valid-endpoint';
        $requestBody = [
            'field_1_name' => 'field_1_value',
            'field_2_name' => 'field_2_value',
        ];
        $requestHeader = [
            'header_variable_1_name' => 'header_variable_1_value',
            'header_variable_2_name' => 'header_variable_2_value',
        ];

        $expectedResponseContent = file_get_contents(__DIR__.'/responses/generic-api-call.json');
        $expectedResponseArray = json_decode($expectedResponseContent, true);

        $this->mockHandler->append(new Response(200, [], $expectedResponseContent));

        $response = $this->photonClient->makeApiCall($method, $endpoint, $requestBody, $requestHeader);

        $this->assertIsArray($response);
        $this->assertEquals($response, $expectedResponseArray);
    }

    /**
     * Test enable caching.
     *
     * @return void
     */
    public function test_enable_caching()
    {
        $response = $this->photonClient->enableCaching();

        $this->assertInstanceOf(PhotonClient::class, $response);
        $this->assertEquals($this->photonClient->cachingEnabled(), true);
    }

    /**
     * Test disable caching.
     *
     * @return void
     */
    public function test_disable_caching()
    {
        $response = $this->photonClient->disableCaching();

        $this->assertInstanceOf(PhotonClient::class, $response);
        $this->assertEquals($this->photonClient->cachingEnabled(), false);
    }
}
