<?php

namespace Photon\Tests\Unit;

use Apix\Cache\Files;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Response;
use Photon\Client as PhotonClient;
use Photon\PhotonException;
use PHPUnit\Framework\TestCase;

abstract class BaseTest extends TestCase
{
    protected $photonClient;

    protected $mockHandler;

    protected $cachingMock;

    protected $email = 'test@email.com';

    /**
     * test setUp method.
     *
     * @return void
     * @throws PhotonException
     */
    protected function setUp(): void
    {
        $this->mockHandler = new MockHandler();
        $httpClient = new GuzzleClient([
            'handler'  => $this->mockHandler,
            'base_uri' => $this->endpoint,
        ]);

        $expectedResponseContent = file_get_contents(__DIR__.'/responses/authenticate.json');
        $this->mockHandler->append(new Response(200, [], $expectedResponseContent));

        $this->cachingMock = $this->createMock(Files::class);

        $this->photonClient = new PhotonClient($this->email, getenv('TEST_PASSWORD'), getenv('TEST_ENDPOINT'), $this->cachingMock, $httpClient);
    }

    /**
     * test tearDown method.
     *
     * @return void
     */
    protected function tearDown(): void
    {
        $this->cachingMock = null;
        $this->mockHandler = null;
        $this->photonClient = null;
    }
}
