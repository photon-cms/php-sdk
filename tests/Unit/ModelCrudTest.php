<?php

namespace Photon\Tests\Unit;

use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use OutOfBoundsException;
use TypeError;

class ModelCrudTest extends BaseTest
{
    /**
     * Test that includedFields attribute is properly set.
     *
     * @return void
     */
    public function test_included_fields_successful_set()
    {
        $includedFieldsInput = ['id', 'first_name', 'last_name'];
        $this->photonClient->setIncludedFields($includedFieldsInput);
        $includedFieldsOutput = $this->photonClient->getIncludedFields();

        $this->assertEquals($includedFieldsInput, $includedFieldsOutput);
    }

    /**
     * Test that includedFields attribute is empty array by default.
     *
     * @return void
     */
    public function test_included_fields_get_empty()
    {
        $includedFieldsOutput = $this->photonClient->getIncludedFields();

        $this->assertIsArray($includedFieldsOutput);
        $this->assertEmpty($includedFieldsOutput);
    }

    /**
     * Test that locales attribute is properly set.
     *
     * @return void
     */
    public function test_locales_successful_set()
    {
        $localesInput = ['en', 'de'];
        $this->photonClient->setLocales($localesInput);
        $localesOutput = $this->photonClient->getLocales();

        $this->assertEquals($localesInput, $localesOutput);
    }

    /**
     * Test that locales attribute is null by default.
     *
     * @return void
     */
    public function test_locales_get_empty()
    {
        $localesOutput = $this->photonClient->getLocales();

        $this->assertEmpty($localesOutput);
    }

    /**
     * Test that model attribute is properly set.
     *
     * @return void
     */
    public function test_model_successful_set()
    {
        $modelInput = 'news';
        $this->photonClient->setModel($modelInput);
        $modelOutput = $this->photonClient->getModel();

        $this->assertEquals($modelInput, $modelOutput);
    }

    /**
     * Test that locales attribute is null by default.
     *
     * @return void
     */
    public function test_model_get_empty()
    {
        $modelOutput = $this->photonClient->getModel();

        $this->assertEmpty($modelOutput);
    }

    /**
     * Test getAll without model being set.
     *
     * @return void
     */
    public function test_get_all_without_model()
    {
        $this->expectException(OutOfBoundsException::class);

        $this->photonClient->getAll();
    }

    /**
     * Test getAll with invalid model name.
     *
     * @return void
     */
    public function test_get_all_invalid_model()
    {
        $this->expectException(ClientException::class);

        $modelInput = 'invalid_model';
        $expectedResponseContent = file_get_contents(__DIR__.'/responses/get-all-invalid-model.json');

        $this->mockHandler->append(
            new ClientException(
                404,
                new Request('GET', $this->endpoint.$modelInput),
                new Response(404, [], $expectedResponseContent)
            )
        );

        $this->photonClient->setModel($modelInput)->getAll();
    }

    /**
     * Test getAll without caching.
     *
     * @return void
     */
    public function test_get_all_without_caching()
    {
        $modelInput = 'news';
        $expectedResponseContent = file_get_contents(__DIR__.'/responses/get-all.json');
        $expectedResponseArray = json_decode($expectedResponseContent, true);

        $this->mockHandler->append(new Response(200, [], $expectedResponseContent));

        $this->cachingMock->expects($this->never())
            ->method('loadKey');
        $this->cachingMock->expects($this->never())
            ->method('save');

        $response = $this->photonClient->setModel($modelInput)->getAll();

        $this->assertIsArray($response);
        $this->assertEquals($response['message'], $expectedResponseArray['message']);
        $this->assertIsArray($response['body']['entries']);
        $this->assertCount(count($expectedResponseArray['body']['entries']), $response['body']['entries']);
    }

    /**
     * Test getAll with caching cache exists.
     *
     * @return void
     */
    public function test_get_all_with_caching_cache_exists()
    {
        $modelInput = 'news';
        $authenticateResponseContent = file_get_contents(__DIR__.'/responses/authenticate.json');
        $authenticateResponseArray = json_decode($authenticateResponseContent, true);
        $cacheKey = json_encode([
            'model'           => $modelInput,
            'id'              => 'all',
            'user_id'         => $authenticateResponseArray['body']['user']['id'],
            'included_fields' => [],
            'filter'          => [],
        ]);

        $expectedResponseContent = file_get_contents(__DIR__.'/responses/get-all.json');
        $expectedResponseArray = json_decode($expectedResponseContent, true);

        $this->cachingMock->expects($this->once())
            ->method('loadKey')
            ->with($cacheKey)
            ->willReturn($expectedResponseArray);
        $this->cachingMock->expects($this->never())
            ->method('save');

        $response = $this->photonClient->setModel($modelInput)->enableCaching()->getAll();

        $this->assertIsArray($response);
        $this->assertEquals($response['message'], $expectedResponseArray['message']);
        $this->assertIsArray($response['body']['entries']);
        $this->assertCount(count($expectedResponseArray['body']['entries']), $response['body']['entries']);
    }

    /**
     * Test getAll with caching cache does not exist.
     *
     * @return void
     */
    public function test_get_all_with_caching_cache_does_not_exists()
    {
        $modelInput = 'news';
        $authenticateResponseContent = file_get_contents(__DIR__.'/responses/authenticate.json');
        $authenticateResponseArray = json_decode($authenticateResponseContent, true);
        $cacheKey = json_encode([
            'model'           => $modelInput,
            'id'              => 'all',
            'user_id'         => $authenticateResponseArray['body']['user']['id'],
            'included_fields' => [],
            'filter'          => [],
        ]);
        $cachingTime = 300;

        $expectedResponseContent = file_get_contents(__DIR__.'/responses/get-all.json');
        $expectedResponseArray = json_decode($expectedResponseContent, true);

        $this->cachingMock->expects($this->once())
            ->method('loadKey')
            ->with($cacheKey)
            ->willReturn(null);
        $this->mockHandler->append(new Response(200, [], $expectedResponseContent));
        $this->cachingMock->expects($this->once())
            ->method('save')
            ->with($expectedResponseArray, $cacheKey, [$modelInput], $cachingTime)
            ->willReturn(true);

        $response = $this->photonClient->setModel($modelInput)->enableCaching()->getAll();

        $this->assertIsArray($response);
        $this->assertEquals($response['message'], $expectedResponseArray['message']);
        $this->assertIsArray($response['body']['entries']);
        $this->assertCount(count($expectedResponseArray['body']['entries']), $response['body']['entries']);
    }

    /**
     * Test get without caching.
     *
     * @return void
     */
    public function test_get_single_entry_without_caching()
    {
        $modelInput = 'news';
        $entryId = 1;

        $expectedResponseContent = file_get_contents(__DIR__.'/responses/get-single-entry.json');
        $expectedResponseArray = json_decode($expectedResponseContent, true);

        $this->mockHandler->append(new Response(200, [], $expectedResponseContent));

        $this->cachingMock->expects($this->never())
            ->method('loadKey');
        $this->cachingMock->expects($this->never())
            ->method('save');

        $response = $this->photonClient->setModel($modelInput)->get($entryId);

        $this->assertIsArray($response);
        $this->assertEquals($response['message'], $expectedResponseArray['message']);
        $this->assertIsArray($response['body']['entry']);
        $this->assertEquals($response['body']['entry']['id'], $entryId);
    }

    /**
     * Test get with caching cache exists.
     *
     * @return void
     */
    public function test_get_single_entry_with_caching_cache_exists()
    {
        $modelInput = 'news';
        $entryId = 1;

        $authenticateResponseContent = file_get_contents(__DIR__.'/responses/authenticate.json');
        $authenticateResponseArray = json_decode($authenticateResponseContent, true);
        $cacheKey = json_encode([
            'model'           => $modelInput,
            'id'              => $entryId,
            'user_id'         => $authenticateResponseArray['body']['user']['id'],
            'included_fields' => [],
            'filter'          => [],
        ]);

        $expectedResponseContent = file_get_contents(__DIR__.'/responses/get-single-entry.json');
        $expectedResponseArray = json_decode($expectedResponseContent, true);

        $this->cachingMock->expects($this->once())
            ->method('loadKey')
            ->with($cacheKey)
            ->willReturn($expectedResponseArray);
        $this->cachingMock->expects($this->never())
            ->method('save');

        $response = $this->photonClient->setModel($modelInput)->enableCaching()->get($entryId);

        $this->assertIsArray($response);
        $this->assertEquals($response['message'], $expectedResponseArray['message']);
        $this->assertIsArray($response['body']['entry']);
        $this->assertEquals($response['body']['entry']['id'], $entryId);
    }

    /**
     * Test get with caching cache does not exist.
     *
     * @return void
     */
    public function test_get_single_entry_with_caching_cache_does_not_exist()
    {
        $modelInput = 'news';
        $entryId = 1;

        $authenticateResponseContent = file_get_contents(__DIR__.'/responses/authenticate.json');
        $authenticateResponseArray = json_decode($authenticateResponseContent, true);
        $cacheKey = json_encode([
            'model'           => $modelInput,
            'id'              => $entryId,
            'user_id'         => $authenticateResponseArray['body']['user']['id'],
            'included_fields' => [],
            'filter'          => [],
        ]);
        $cachingTime = 300;

        $expectedResponseContent = file_get_contents(__DIR__.'/responses/get-single-entry.json');
        $expectedResponseArray = json_decode($expectedResponseContent, true);

        $this->cachingMock->expects($this->once())
            ->method('loadKey')
            ->with($cacheKey)
            ->willReturn(null);
        $this->mockHandler->append(new Response(200, [], $expectedResponseContent));
        $this->cachingMock->expects($this->once())
            ->method('save')
            ->with($expectedResponseArray, $cacheKey, [$modelInput], $cachingTime)
            ->willReturn(true);

        $response = $this->photonClient->setModel($modelInput)->enableCaching()->get($entryId);

        $this->assertIsArray($response);
        $this->assertEquals($response['message'], $expectedResponseArray['message']);
        $this->assertIsArray($response['body']['entry']);
        $this->assertEquals($response['body']['entry']['id'], $entryId);
    }

    /**
     * Test filter without caching.
     *
     * @return void
     */
    public function test_filter_without_caching()
    {
        $modelInput = 'news';
        $filterData = [
            'filter' => [
                'title' => [
                    'en' => ['begins_with' => 'Title'],
                ],
            ],
            'pagination' => [
                'items_per_page' => 2,
                'current_page'   => 2,
            ],
        ];

        $expectedResponseContent = file_get_contents(__DIR__.'/responses/filter.json');
        $expectedResponseArray = json_decode($expectedResponseContent, true);

        $this->mockHandler->append(new Response(200, [], $expectedResponseContent));

        $this->cachingMock->expects($this->never())
            ->method('loadKey');
        $this->cachingMock->expects($this->never())
            ->method('save');

        $response = $this->photonClient->setModel($modelInput)->disableCaching()->filter($filterData);

        $this->assertIsArray($response);

        $this->assertEquals($response['message'], $expectedResponseArray['message']);

        $this->assertIsArray($response['body']['entries']);
        $this->assertEquals($response['body']['entries'], $expectedResponseArray['body']['entries']);
        $this->assertCount($filterData['pagination']['items_per_page'], $expectedResponseArray['body']['entries']);

        $this->assertIsArray($response['body']['pagination']);
        $this->assertEquals($response['body']['pagination']['count'], $filterData['pagination']['items_per_page']);
        $this->assertEquals($response['body']['pagination']['current_page'], $filterData['pagination']['current_page']);
    }

    /**
     * Test filter with caching cache exists.
     *
     * @return void
     */
    public function test_filter_with_caching_cache_exists()
    {
        $modelInput = 'news';
        $filterData = [
            'filter' => [
                'title' => [
                    'en' => ['begins_with' => 'Title'],
                ],
            ],
            'pagination' => [
                'items_per_page' => 2,
                'current_page'   => 2,
            ],
        ];

        $authenticateResponseContent = file_get_contents(__DIR__.'/responses/authenticate.json');
        $authenticateResponseArray = json_decode($authenticateResponseContent, true);
        $cacheKey = json_encode([
            'model'   => $modelInput,
            'id'      => 'filter',
            'user_id' => $authenticateResponseArray['body']['user']['id'],
            'filter'  => $filterData,
        ]);

        $expectedResponseContent = file_get_contents(__DIR__.'/responses/filter.json');
        $expectedResponseArray = json_decode($expectedResponseContent, true);

        $this->cachingMock->expects($this->once())
            ->method('loadKey')
            ->with($cacheKey)
            ->willReturn($expectedResponseArray);
        $this->cachingMock->expects($this->never())
            ->method('save');

        $response = $this->photonClient->setModel($modelInput)->enableCaching()->filter($filterData);

        $this->assertIsArray($response);

        $this->assertEquals($response['message'], $expectedResponseArray['message']);

        $this->assertIsArray($response['body']['entries']);
        $this->assertEquals($response['body']['entries'], $expectedResponseArray['body']['entries']);
        $this->assertCount($filterData['pagination']['items_per_page'], $expectedResponseArray['body']['entries']);

        $this->assertIsArray($response['body']['pagination']);
        $this->assertEquals($response['body']['pagination']['count'], $filterData['pagination']['items_per_page']);
        $this->assertEquals($response['body']['pagination']['current_page'], $filterData['pagination']['current_page']);
    }

    /**
     * Test filter with caching cache does not exist.
     *
     * @return void
     */
    public function test_filter_with_caching_cache_does_not_exist()
    {
        $modelInput = 'news';
        $filterData = [
            'filter' => [
                'title' => [
                    'en' => ['begins_with' => 'Title'],
                ],
            ],
            'pagination' => [
                'items_per_page' => 2,
                'current_page'   => 2,
            ],
        ];

        $authenticateResponseContent = file_get_contents(__DIR__.'/responses/authenticate.json');
        $authenticateResponseArray = json_decode($authenticateResponseContent, true);
        $cacheKey = json_encode([
            'model'   => $modelInput,
            'id'      => 'filter',
            'user_id' => $authenticateResponseArray['body']['user']['id'],
            'filter'  => $filterData,
        ]);
        $cachingTime = 300;

        $expectedResponseContent = file_get_contents(__DIR__.'/responses/filter.json');
        $expectedResponseArray = json_decode($expectedResponseContent, true);

        $this->cachingMock->expects($this->once())
            ->method('loadKey')
            ->with($cacheKey)
            ->willReturn(null);
        $this->mockHandler->append(new Response(200, [], $expectedResponseContent));
        $this->cachingMock->expects($this->once())
            ->method('save')
            ->with($expectedResponseArray, $cacheKey, [$modelInput], $cachingTime)
            ->willReturn(true);

        $response = $this->photonClient->setModel($modelInput)->enableCaching()->filter($filterData);

        $this->assertIsArray($response);

        $this->assertEquals($response['message'], $expectedResponseArray['message']);

        $this->assertIsArray($response['body']['entries']);
        $this->assertEquals($response['body']['entries'], $expectedResponseArray['body']['entries']);
        $this->assertCount($filterData['pagination']['items_per_page'], $expectedResponseArray['body']['entries']);

        $this->assertIsArray($response['body']['pagination']);
        $this->assertEquals($response['body']['pagination']['count'], $filterData['pagination']['items_per_page']);
        $this->assertEquals($response['body']['pagination']['current_page'], $filterData['pagination']['current_page']);
    }

    /**
     * Test count.
     *
     * @return void
     */
    public function test_count()
    {
        $modelInput = 'news';

        $expectedResponseContent = file_get_contents(__DIR__.'/responses/count.json');
        $expectedResponseArray = json_decode($expectedResponseContent, true);

        $this->mockHandler->append(new Response(200, [], $expectedResponseContent));

        $response = $this->photonClient->setModel($modelInput)->count();

        $this->assertIsArray($response);
        $this->assertEquals($response['message'], $expectedResponseArray['message']);

        $this->assertIsArray($response['body']);
        $this->assertEquals($response['body']['count'], $expectedResponseArray['body']['count']);
    }

    /**
     * Test create without caching.
     *
     * @return void
     */
    public function test_create_without_caching()
    {
        $modelInput = 'news';
        $createData = [
            'title' => [
                'en' => 'Title 1',
                'sr' => 'Naslov 1',
            ],
            'heading' => [
                'en' => 'Heading 1',
            ],
        ];

        $expectedResponseContent = file_get_contents(__DIR__.'/responses/create-single-entry.json');
        $expectedResponseArray = json_decode($expectedResponseContent, true);

        $this->mockHandler->append(new Response(200, [], $expectedResponseContent));

        $this->cachingMock->expects($this->never())
            ->method('clean');

        $response = $this->photonClient->disableCaching()->setModel($modelInput)->create($createData);

        $this->assertIsArray($response);
        $this->assertEquals($response, $expectedResponseArray);
        $this->assertEquals($createData['title']['en'], $response['body']['entry']['title']['en']);
        $this->assertEquals($createData['title']['sr'], $response['body']['entry']['title']['sr']);
        $this->assertEquals($createData['heading']['en'], $response['body']['entry']['heading']['en']);
    }

    /**
     * Test create with caching.
     *
     * @return void
     */
    public function test_create_with_caching()
    {
        $modelInput = 'news';
        $createData = [
            'title' => [
                'en' => 'Title 1',
                'sr' => 'Naslov 1',
            ],
            'heading' => [
                'en' => 'Heading 1',
            ],
        ];

        $expectedResponseContent = file_get_contents(__DIR__.'/responses/create-single-entry.json');
        $expectedResponseArray = json_decode($expectedResponseContent, true);

        $this->mockHandler->append(new Response(200, [], $expectedResponseContent));

        $this->cachingMock->expects($this->once())
            ->method('clean')
            ->with([$modelInput])
            ->willReturn(true);

        $response = $this->photonClient->enableCaching()->setModel($modelInput)->create($createData);

        $this->assertIsArray($response);
        $this->assertEquals($response, $expectedResponseArray);
        $this->assertEquals($createData['title']['en'], $response['body']['entry']['title']['en']);
        $this->assertEquals($createData['title']['sr'], $response['body']['entry']['title']['sr']);
        $this->assertEquals($createData['heading']['en'], $response['body']['entry']['heading']['en']);
    }

    /**
     * Test update without caching.
     *
     * @return void
     */
    public function test_update_without_caching()
    {
        $modelInput = 'news';
        $updateId = 1;
        $updateData = [
            'title' => [
                'en' => 'Title 1 update',
            ],
            'heading' => [
                'en' => 'Heading 1 update',
            ],
        ];

        $expectedResponseContent = file_get_contents(__DIR__.'/responses/update-single-entry.json');
        $expectedResponseArray = json_decode($expectedResponseContent, true);

        $this->mockHandler->append(new Response(200, [], $expectedResponseContent));

        $this->cachingMock->expects($this->never())
            ->method('clean');

        $response = $this->photonClient->disableCaching()->setModel($modelInput)->update($updateId, $updateData);

        $this->assertIsArray($response);
        $this->assertEquals($response, $expectedResponseArray);
        $this->assertEquals($updateId, $response['body']['entry']['id']);
        $this->assertEquals($updateData['title']['en'], $response['body']['entry']['title']['en']);
        $this->assertEquals($updateData['heading']['en'], $response['body']['entry']['heading']['en']);
    }

    /**
     * Test update with caching.
     *
     * @return void
     */
    public function test_update_with_caching()
    {
        $modelInput = 'news';
        $updateId = 1;
        $updateData = [
            'title' => [
                'en' => 'Title 1 update',
            ],
            'heading' => [
                'en' => 'Heading 1 update',
            ],
        ];

        $expectedResponseContent = file_get_contents(__DIR__.'/responses/update-single-entry.json');
        $expectedResponseArray = json_decode($expectedResponseContent, true);

        $this->mockHandler->append(new Response(200, [], $expectedResponseContent));

        $this->cachingMock->expects($this->once())
            ->method('clean')
            ->with([$modelInput])
            ->willReturn(true);

        $response = $this->photonClient->enableCaching()->setModel($modelInput)->update($updateId, $updateData);

        $this->assertIsArray($response);
        $this->assertEquals($response, $expectedResponseArray);
        $this->assertEquals($updateId, $response['body']['entry']['id']);
        $this->assertEquals($updateData['title']['en'], $response['body']['entry']['title']['en']);
        $this->assertEquals($updateData['heading']['en'], $response['body']['entry']['heading']['en']);
    }

    /**
     * Test delete witohut caching.
     *
     * @return void
     */
    public function test_delete_without_caching()
    {
        $modelInput = 'news';
        $deleteId = 1;

        $expectedResponseContent = file_get_contents(__DIR__.'/responses/delete-single-entry.json');
        $expectedResponseArray = json_decode($expectedResponseContent, true);

        $this->mockHandler->append(new Response(200, [], $expectedResponseContent));

        $this->cachingMock->expects($this->never())
            ->method('clean');

        $response = $this->photonClient->disableCaching()->setModel($modelInput)->delete($deleteId);

        $this->assertIsArray($response);
        $this->assertEquals($response, $expectedResponseArray);
        $this->assertEquals($response['message'], 'DELETE_DYNAMIC_MODULE_ENTRY_SUCCESS');
    }

    /**
     * Test delete with caching.
     *
     * @return void
     */
    public function test_delete_with_caching()
    {
        $modelInput = 'news';
        $deleteId = 1;

        $expectedResponseContent = file_get_contents(__DIR__.'/responses/delete-single-entry.json');
        $expectedResponseArray = json_decode($expectedResponseContent, true);

        $this->mockHandler->append(new Response(200, [], $expectedResponseContent));

        $this->cachingMock->expects($this->once())
            ->method('clean')
            ->with([$modelInput])
            ->willReturn(true);

        $response = $this->photonClient->enableCaching()->setModel($modelInput)->delete($deleteId);

        $this->assertIsArray($response);
        $this->assertEquals($response, $expectedResponseArray);
        $this->assertEquals($response['message'], 'DELETE_DYNAMIC_MODULE_ENTRY_SUCCESS');
    }
}
