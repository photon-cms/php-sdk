<?php

namespace Photon\Tests\Unit;

use GuzzleHttp\Psr7\Response;
use Photon\Client as PhotonClient;

class ImpersonateUserTest extends BaseTest
{
    /**
     * Test impersonate.
     *
     * @return void
     */
    public function test_impersonate()
    {
        $impersonateUserId = 123;
        $expectedResponseContent = file_get_contents(__DIR__.'/responses/impersonate.json');

        $this->mockHandler->append(new Response(200, [], $expectedResponseContent));

        $impersonateOutput = $this->photonClient->impersonate($impersonateUserId);

        $this->assertInstanceOf(PhotonClient::class, $impersonateOutput);
    }

    /**
     * Test impersonate stop.
     *
     * @return void
     */
    public function test_impersonate_stop()
    {
        $expectedResponseContent = file_get_contents(__DIR__.'/responses/impersonate-stop.json');

        $this->mockHandler->append(new Response(200, [], $expectedResponseContent));

        $impersonateOutput = $this->photonClient->stopImpersonate();

        $this->assertInstanceOf(PhotonClient::class, $impersonateOutput);
    }
}
